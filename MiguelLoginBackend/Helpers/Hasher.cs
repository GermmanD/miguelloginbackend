﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Helpers
{
    public static class Hasher
    {
        // Método que hashea un string aplicando el algoritmo SHA256.
        public static string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();

            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] hashedValue = hashAlgorithm.ComputeHash(byteValue);

            return Convert.ToBase64String(hashedValue);
        }
    }
}
