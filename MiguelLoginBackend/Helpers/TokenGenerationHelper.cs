﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MiguelLoginBackend.Infrastructure;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Helpers
{
    // Clase que ayudará la creación de tokens JWT.
    public static class TokenGenerationHelper
    {
        // Método que devuelve un arreglo de Claims dado un ApplicationUser.
        public static Claim[] GenerateClaims(ApplicationUser user)
        {
            return new Claim[]
            {
                new Claim("id", user.Id),
                new Claim("email", user.Email),
                new Claim("phonenumber", user.PhoneNumber ?? "00-000-0000000"),
                new Claim("username", user.UserName)
            };
        }

        // Clase que devuelve ya el token en cadena del ApplicationUser.
        public static JwtSecurityToken GenerateToken(ApplicationUser user, IConfiguration Configuration)
        {
            return new JwtSecurityToken
                (
                    issuer: Configuration["JwtIssuer"],
                    audience: Configuration["JwtAudience"],
                    claims: GenerateClaims(user),
                    expires: DateTime.UtcNow.AddMinutes(30),
                    notBefore: DateTime.UtcNow,
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])), 
                        SecurityAlgorithms.HmacSha256)
                );
        }
    }
}
