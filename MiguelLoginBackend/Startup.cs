﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MiguelLoginBackend.Infrastructure;

namespace MiguelLoginBackend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // Éste método se llama en tiempo de ejecución y se usa para registrar servicios.
        public void ConfigureServices(IServiceCollection services)
        {
            // Se registra el contexto de la base de datos para ser inyectado en los controladores.
            services.AddDbContext<ApplicationDbContext>(options => 
                options.UseSqlServer(Configuration.GetConnectionString("LoginBackendDB")));

            // Se agrega la configuración por defecto del sistema de identidad a las plantillas especificadas.
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Se configuran las opciones de identidad de Identity.
            services.Configure<IdentityOptions>(options =>
            {
                // Configuración de Contraseña.
                options.Password.RequiredLength = 8;
                options.Password.RequireDigit = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;

                // Configuración de bloqueo de identidad.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;

                // Configuración de Usuario.
                options.User.RequireUniqueEmail = true;
            });

            // Se agrega soporte de validación de tokens de acceso JWT. 
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    // Se configuran los parámetros de validación del token.
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateActor = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"]))
                    };
                });

            // Se agrega soporte a CORS para permitir al frontend hacer peticiones a la API.
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", builder =>
                {
                    builder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
                });
            });

            services.AddMvc();
        }

        // Éste método se llama en tiempo de ejecución para configurar el pipe de peticiones HTTP.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Se habilita el uso de CORS de la política que se ha creado previamente.
            app.UseCors("AllowAll");
            // Se habilita el Middleware de autentificación de Identity al pipeline de peticiones HTTP.
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
