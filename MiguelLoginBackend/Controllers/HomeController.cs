﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MiguelLoginBackend.Infrastructure;
using MiguelLoginBackend.Models;
using MiguelLoginBackend.Models.DTO;
using Newtonsoft.Json;

namespace MiguelLoginBackend.Controllers
{
    [Produces("application/json")]
    [Route("api/Home")]
    public class HomeController : Controller
    {
        public ApplicationDbContext DbContext { get; set; }
        public UserManager<ApplicationUser> UserManager { get; set; }
        public IConfiguration Configuration { get; set; }

        // Se inyectan los servicios del contexto de la base de datos y configuración de la aplicación.
        public HomeController(ApplicationDbContext dbContext, UserManager<ApplicationUser> userManager, IConfiguration configuration)
        {
            DbContext = dbContext;
            UserManager = userManager;
            Configuration = configuration;
        }

        // Acción para la creación de un nuevo usuario.
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateUser([FromBody]CreateUserBindingModel userDTO)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if((await UserManager.FindByEmailAsync(userDTO.Email)) != null)
            {
                return BadRequest(JsonConvert.SerializeObject(new { message = "Email already registered." }));
            }

            ApplicationUser userContext = CreateUserBindingModel.GetUserContext(userDTO);
            IdentityResult result = await UserManager.CreateAsync(userContext, userDTO.Password);
            if(result != IdentityResult.Success)
            {
                return BadRequest(result);
            }

            UserModelDTO userModelDTO = UserModelDTO.GetUserModelDTO(userContext);
            
            return Created("api/Home", userModelDTO);
        }
    }
}