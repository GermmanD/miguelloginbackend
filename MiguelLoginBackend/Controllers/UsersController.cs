﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MiguelLoginBackend.Infrastructure;
using MiguelLoginBackend.Models;
using Newtonsoft.Json;

namespace MiguelLoginBackend.Controllers
{
    // Controlador donde se manejará toda la lógica que tenga que ver directamente con las entidades de los usuarios.
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        public ApplicationDbContext DbContext { get; set; }
        public UserManager<ApplicationUser> UserManager { get; set; }
        public IConfiguration Configuration { get; set; }

        // Se inyectan los servicios del contexto de la base de datos y configuración de la aplicación.
        public UsersController(ApplicationDbContext dbContext, UserManager<ApplicationUser> userManager, IConfiguration configuration)
        {
            DbContext = dbContext;
            UserManager = userManager;
            Configuration = configuration;
        }

        // Acción para obtener un usuario por id.
        // Nótese el: "[Authorize(AuthenticationSchemes = "Bearer")]"
        // Todos los controladores con este filtro estarán protegidos.
        // Para poder acceder a ellos se debe enviar el token de acceso en los headers de la petición HTTP.
        // De lo contrario devolverá acceso no autorizado.
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Route("{id}")]
        public async Task<IActionResult> GetUserById(string id)
        {
            ApplicationUser user = await UserManager.FindByIdAsync(id);
            if(user == null)
            {
                return BadRequest(JsonConvert.SerializeObject(new { message = "The user does not exist" }));
            }

            UserModelDTO userDTO = UserModelDTO.GetUserModelDTO(user);
            return Ok(userDTO);
        }
    }
}