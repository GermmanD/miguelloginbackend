﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MiguelLoginBackend.Helpers;
using MiguelLoginBackend.Infrastructure;
using MiguelLoginBackend.Models;
using MiguelLoginBackend.Models.DTO;
using Newtonsoft.Json;

namespace MiguelLoginBackend.Controllers
{
    // Controlador para autenficación de usuarios.
    // Éste es el controlador que se tiene que llamar para validar credenciales (Login) y obtener los tokens
    // O para renovar el token de refresco.
    [Produces("application/json")]
    [Route("api/OAuth")]
    public class OAuthController : Controller
    {
        public ApplicationDbContext DbContext { get; set; }
        public UserManager<ApplicationUser> UserManager { get; set; }
        public IConfiguration Configuration { get; set; }

        // Se inyectan los servicios del contexto de la base de datos y configuración de la aplicación.
        public OAuthController(ApplicationDbContext dbContext, UserManager<ApplicationUser> userManager, IConfiguration configuration)
        {
            DbContext = dbContext;
            UserManager = userManager;
            Configuration = configuration;
        }

        // Acción para obtener tokens de acceso y de refresco dado credenciales.
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]LoginModelDTO loginData)
        {
            // Se validan los filtros de loginData.
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Se verifican las credenciales.
            ApplicationUser user = await UserManager.FindByEmailAsync(loginData.Email);
            if(user == null || !(await UserManager.CheckPasswordAsync(user, loginData.Password)))
            {
                return BadRequest("Email or Password incorrect.");
            }
            // Si las credenciales son correctos, se carga la entidad del token de refresco al que corresponse el usuario
            // a través del "Explicit Loading".
            DbContext.Entry(user).Reference(u => u.RefreshToken).Load();

            // Se crea el nuevo token de acceso del usuario.
            JwtSecurityToken userToken = TokenGenerationHelper.GenerateToken(user, Configuration);
            string accessToken = (new JwtSecurityTokenHandler()).WriteToken(userToken);

            // Se verifica si el usuario ya tenía un token de refresco previo, de ser así, se elimina y se crea uno nuevo.
            RefreshTokenModel userRefreshToken = user.RefreshToken;
            if(userRefreshToken != null)
            {
                DbContext.RefreshTokens.Remove(userRefreshToken);
            }

            // Se crea un nuevo token de refresco para el usuario.
            string refreshTokenId = Guid.NewGuid().ToString("n");
            RefreshTokenModel nRefreshToken = new RefreshTokenModel()
            {
                RefreshTokenId = Hasher.GetHash(refreshTokenId),
                User = user,
                ExpirationDate = DateTime.UtcNow.AddDays(7)
            };

            // Se agrega el nuevo token de refresco y se guardan los cambios en la base de datos.
            await DbContext.RefreshTokens.AddAsync(nRefreshToken);
            await DbContext.SaveChangesAsync();

            // Se devuelve tanto el token de acceso como el token de refresco.
            // La duración del token de acceso es de 30 minutos, mientras que el de refresco es de 7 días.
            return Ok(new {
                access_token = accessToken,
                expires_in = DateTime.UtcNow.AddMinutes(30).ToString("MM/dd/yyyyy HH:mm"),
                user_id = user.Id,
                refresh_token = refreshTokenId
            });
        }

        // Acción para renovar el token de refresco y el token de acceso dado el token de refresco y el id de usuario.
        [HttpPost]
        [Route("Refresh")]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshToken([FromBody]RefreshTokenDTO refreshData)
        {
            // Se valida que el token de refresco exista, que no haya expirado y si el id que se envío corresponde con el mismo id
            // con el que fue emitido.
            RefreshTokenModel refreshToken = await DbContext.RefreshTokens.FindAsync(Hasher.GetHash(refreshData.RefreshToken));
            if(refreshToken == null)
            {
                return BadRequest(JsonConvert.SerializeObject(new { message = "Invalid Refresh Token." }));
            } else if(DateTime.Compare(refreshToken.ExpirationDate, DateTime.UtcNow) < 0)
            {
                return BadRequest(JsonConvert.SerializeObject(new { message = "Refresh Token has expired." }));
            } else if(!refreshToken.UserId.Equals(refreshData.UserId))
            {
                return BadRequest(JsonConvert.SerializeObject(new { message = "User id doesn't match with the issuer one." }));
            }

            // Si se llegó hsata aquí, es porque todo estuvo bien y el token de refresco existe, por lo que se elimina para crear uno nuevo.
            DbContext.RefreshTokens.Remove(refreshToken);

            // Se obtiene el usuario con el id enviado en la petición.
            ApplicationUser user = await UserManager.FindByIdAsync(refreshData.UserId);

            // Se crea el nuevo token de acceso.
            JwtSecurityToken userToken = TokenGenerationHelper.GenerateToken(user, Configuration);
            string accessToken = (new JwtSecurityTokenHandler()).WriteToken(userToken);

            // Se crea el nuevo token de refresco.
            string refreshTokenId = Guid.NewGuid().ToString("n");
            RefreshTokenModel nRefreshToken = new RefreshTokenModel()
            {
                RefreshTokenId = Hasher.GetHash(refreshTokenId),
                User = user,
                ExpirationDate = DateTime.UtcNow.AddDays(7)
            };

            // Se agrega el nuevo token de refresco a la base de datos y se guardan los cambios.
            await DbContext.RefreshTokens.AddAsync(nRefreshToken);
            await DbContext.SaveChangesAsync();

            // Se retorna tanto el token de acceso como el token de refresco.
            return Ok(new {
                access_token = accessToken,
                expires_in = DateTime.UtcNow.AddMinutes(30).ToString("MM/dd/yyyyy HH:mm"),
                user_id = user.Id,
                refresh_token = refreshTokenId
            });
        }
    }
}