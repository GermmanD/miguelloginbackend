﻿using MiguelLoginBackend.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Models
{
    // Clase que representa la relación de RefreshTokens en la base de datos.
    public class RefreshTokenModel
    {
        public string RefreshTokenId { get; set; }
        public DateTime ExpirationDate { get; set; }
        // Se agrega un ApplicationUser ya que existe una relación One-To-One especificada en RefreshTokenModelConfiguration.
        public virtual ApplicationUser User { get; set; }
        public string UserId { get; set; }
    }
}
