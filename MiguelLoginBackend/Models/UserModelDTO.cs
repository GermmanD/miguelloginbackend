﻿using MiguelLoginBackend.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Models
{
    // Modelo de transferencia de usuarios.
    public class UserModelDTO
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool PhoneNumberConfirmed { get; set; }

        // Método que devuelve un UserModelDTO dado un ApplicationUser.
        public static UserModelDTO GetUserModelDTO(ApplicationUser user)
        {
            return new UserModelDTO()
            {
                UserId = user.Id,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Username = user.UserName,
                EmailConfirmed = user.EmailConfirmed,
                PhoneNumberConfirmed = user.PhoneNumberConfirmed
            };
        }
    }
}
