﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Models.Configurations
{
    public class RefreshTokenModelConfiguration : IEntityTypeConfiguration<RefreshTokenModel>
    {
        public RefreshTokenModelConfiguration()
        {

        }

        public void Configure(EntityTypeBuilder<RefreshTokenModel> builder)
        {
            // Se establece la llave primaria de RefreshTokenModel.
            builder.HasKey(r => r.RefreshTokenId);

            // Se hace la propiedad ExpirationDate (DateTime) como requerida.
            // Ésta propiedad es la que indicará si el token de refresco ha expirado.
            // De ser así, el token de refresco queda invalidado.
            builder.Property(r => r.ExpirationDate)
                .IsRequired();

            // Se crea la relación One-To-One entre ApplicationUser y RefreshTokenModel.
            builder.HasOne(r => r.User)
                .WithOne(u => u.RefreshToken)
                .HasForeignKey<RefreshTokenModel>(r => r.UserId);
        }
    }
}
