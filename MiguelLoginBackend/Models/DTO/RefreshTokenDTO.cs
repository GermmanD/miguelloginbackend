﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Models.DTO
{
    // Modelo de transferencia que se usará cuando se desee renovar el token.
    public class RefreshTokenDTO
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string RefreshToken { get; set; }
    }
}
