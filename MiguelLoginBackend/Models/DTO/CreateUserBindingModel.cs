﻿using MiguelLoginBackend.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Models.DTO
{
    // Clase modelo para la creación de usuarios.
    public class CreateUserBindingModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }

        // Método que devuelve un ApplicationUser dado un CreateUserBindingModel
        public static ApplicationUser GetUserContext(CreateUserBindingModel user)
        {
            return new ApplicationUser()
            {
                UserName = user.Username,
                Email = user.Email
            };
        }
    }
}
