﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Models.DTO
{
    // Modelo de transferencia que se usará para validar las credenciales de un usuario la hacer login.
    public class LoginModelDTO
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [MinLength(8)]
        public string Password { get; set; }
    }
}
