﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MiguelLoginBackend.Models;
using MiguelLoginBackend.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Infrastructure
{
    // Clase que representará al contexto de la base de datos de la API.
    // TIENE QUE HEREDAR DE IdentityDbContext ya que ésta provee el contexto de la base de datos con las funcionales de Identity.
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {

        }

        // Relación de la base de datos que contendrá los tokens de refresco.
        public virtual DbSet<RefreshTokenModel> RefreshTokens { get; set; }

        // Se hace uso de la Fluent API para modelar las relaciones de la base de datos.
        // Las configuraciones se harán en clases aparte para mantener el método "OnModelCreating" limpio y leíble.
        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Se aplica la configuración definida en RefreshTokenModelConfiguration a la relación de RefreshTokens.
            builder.ApplyConfiguration(new RefreshTokenModelConfiguration());

            base.OnModelCreating(builder);
        }
    }
}
