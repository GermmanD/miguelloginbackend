﻿using Microsoft.AspNetCore.Identity;
using MiguelLoginBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiguelLoginBackend.Infrastructure
{
    // Relación que hará referencia a los usuarios del sistema registrados en la base de datos.
    // TIENE QUE HEREDAR DE IdentityUser ya que ésta clase contiene los campos que Identity provee para el manejo de usuarios.
    public class ApplicationUser : IdentityUser
    {
        // Se le anexa un RefreshTokenModel ya que existirá una Relación One-To-One con dicha relación.
        // Esta relación está definida en la configuración de RefreshTokenModelConfiguration.
        public virtual RefreshTokenModel RefreshToken { get; set; }
    }
}
